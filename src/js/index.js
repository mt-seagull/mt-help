(function() {
	var div = document.querySelector('[data-viewform]')
	if (!div) return
  var a = document.createElement('a')
  a.target = '_blank'
  a.rel = 'noopener noreferrer'
  var url = new URL('https://docs.google.com/forms/d/e/1FAIpQLSfMoIylZZtA-OcUi-sNCXtWpfCuef9uq_FEwWGlj6rqvHeDYw/viewform')
  url.searchParams.set('entry.143542336', location.href)
  a.href = url.toString()
  a.textContent = 'お問い合わせ'
  div.appendChild(a)
})()
